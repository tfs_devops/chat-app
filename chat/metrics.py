import time

from flask import Flask, request, current_app, g
from prometheus_client import Histogram, Counter
from prometheus_client import multiprocess
from prometheus_client.exposition import generate_latest
from prometheus_client.registry import CollectorRegistry


class Metrics:
    """Route for application metrics exposure.
    Further reading: https://medium.com/@andrei.chernyshev/prometheus-python-flask-8487c3bc5b36

    Example:

        app = Flask(__name__)
        metrics = Metrics(url='/metrics', endpoint='metrics')
        metrics.init_app()

        or just

        app = Flask(__name__)
        Metrics(app)

    Gunicorn conf example:

    def worker_exit(server, worker):
        from prometheus_client import multiprocess
        multiprocess.mark_process_dead(worker.pid)

    $ gunicorn -c conf.py ...
    """

    def __init__(self, app: Flask = None):
        self.registry = CollectorRegistry(auto_describe=True)
        self.request_latency = Histogram(
            name='request_latency_seconds',
            documentation='Flask Request Latency',
            labelnames=['method', 'path'],
            registry=self.registry
        )

        self.request_count = Counter(
            name='flask_request_count',
            documentation='Flask Request Count',
            labelnames=['method', 'path', 'http_status'],
            registry=self.registry
        )

        if app is not None:
            self.init_app(app)

    def init_app(self, app: Flask):
        app.extensions['metrics'] = self

        app.before_request(self._remember_start_time)
        app.after_request(self._update_stats_registry)

        url_rule = '/metrics'
        endpoint = 'metrics'

        app.add_url_rule(
            rule=url_rule,
            endpoint=endpoint,
            view_func=self._stats_endpoint
        )

        # Requires gunicorn config
        multiprocess_enabled = app.config.get('METRICS_MULTIPROCESS')
        if multiprocess_enabled:
            self._setup_multiprocess_collection(app)

    def _remember_start_time(self):
        """Remember request start time."""
        start_time = time.time()
        g.start_time = start_time

        current_app.logger.debug(
            'Setting start time for request %s',
            request.headers.get('Uber-Trace-Id')
        )

    def _update_stats_registry(self, response):
        """Update internal prometheus registry."""
        try:
            request_latency = time.time() - g.start_time
            self.request_latency.labels(request.method,
                                        request.path) \
                .observe(request_latency)
            self.request_count.labels(request.method,
                                      request.path,
                                      response.status_code).inc()

        except AttributeError:
            current_app.logger.error('Unable to get request start time')
            current_app.logger.error(request.headers)

        finally:
            return response

    def _stats_endpoint(self):
        """Flask view function to expose metrics."""
        return generate_latest(self.registry)

    def _setup_multiprocess_collection(self, app: Flask):
        multiprocess.MultiProcessCollector(
            registry=self.registry,
            path=app.config.get('METRICS_TEMP_DIR')
        )